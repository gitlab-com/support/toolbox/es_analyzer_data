# es_analyzer_data

This is the es data that is pulled by the es_analyzer.
https://gitlab.com/gitlab-com/support/toolbox/es_analyzer#gitlab-elasticsearch-analyzer

It containes an Elasticsearch 6.3.2 index that has been created by a selfhosted GitLab instance.

It is being used so that the es_analyzer does not require a GitLab instance to run.
