FROM ubuntu:18.04

RUN apt update
RUN apt install openjdk-8-jre-headless wget curl nano -y

RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-oss-6.3.2.tar.gz
RUN tar xvf elasticsearch-oss-6.3.2.tar.gz
RUN cd elasticsearch-6.3.2
RUN bin/elasticsearch &
#RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-oss-6.3.2.deb
#RUN dpkg -i elasticsearch-oss-6.3.2.deb

RUN echo 'copying data'
COPY data/ /elasticsearch-6.3.2/data/

RUN groupadd -r elasticsearch && useradd -r -g elasticsearch elasticsearch
RUN chown -R elasticsearch:elasticsearch /elasticsearch-6.3.2

USER elasticsearch

WORKDIR /elasticsearch-6.3.2/bin

RUN echo 'network.host: 0.0.0.0' >> /elasticsearch-6.3.2/config/elasticsearch.yml

EXPOSE 9200 9300

CMD ["/elasticsearch-6.3.2/bin/elasticsearch"]
